import logo from "./logo.svg";
import "./App.css";
import { Card } from "./Card";

function App() {
  return (
    <div className="App">
      <Card
        thumbnail="https://www.my-loire-valley.com/wp-content/uploads/2018/10/Ile_de_Nantes-Jibi44-cc.jpg"
        userPicture="https://api.time.com/wp-content/uploads/2015/03/rtr4v2mv.jpg?quality=85"
        userName="The queen"
        projectName="God save the queen"
      />
    </div>
  );
}

export default App;
