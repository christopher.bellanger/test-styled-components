import styled from "styled-components";

export const Block = styled.div`
  width: 30rem;
  background-color: white;
  margin: auto;
`;

export const Thumbnail = styled.div`
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  background-size: cover;
  height: 14rem;
  background-image: ${({ thumbnail }) => `url(${thumbnail})`};
`;

export const ErrorPreview = styled.div`
  background-color: purple;
  height: 14rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: grey;
  font-style: italic;
`;

export const UserInfoContainer = styled.div`
  position: relative;
  padding: 0.5rem 1.6rem 0.3rem 6.3rem;
  margin-bottom: 1rem;
`;

export const UserImage = styled.img`
  position: absolute;
  bottom: 0;
  left: 1.2rem;
  height: 4rem;
  width: 4rem;
  border-radius: 10rem;
  background-color: #e2e2e2;
`;

export const Elipsis = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const UserNameContainer = styled(Elipsis)`
  font-weight: bold;
  color: purple;
`;

export const ProductNameContainer = styled(Elipsis)`
  color: blueviolet;
`;

export const ProjectNameContainer = styled(Elipsis)`
  color: red;
  padding: 1rem;
`;
