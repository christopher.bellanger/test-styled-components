import {
  Block,
  ErrorPreview,
  ProductNameContainer,
  Thumbnail,
  UserInfoContainer,
  UserNameContainer,
  ProjectNameContainer,
  UserImage,
} from "./components";

export const Card = ({
  thumbnail = "",
  userPicture = "",
  userName = "",
  projectName = "",
  productTitle = "",
  onClick = () => {},
}) => {
  return (
    <Block onClick={onClick} padding="NONE">
      <div>
        {thumbnail ? (
          <Thumbnail thumbnail={thumbnail} />
        ) : (
          <ErrorPreview>No preview picture</ErrorPreview>
        )}
      </div>
      <UserInfoContainer>
        <UserImage src={userPicture} alt="" />
        <UserNameContainer>
          <span>{userName}</span>
        </UserNameContainer>
      </UserInfoContainer>
      <ProductNameContainer>
        <span>{productTitle ? productTitle : "Product undefined"}</span>
      </ProductNameContainer>
      <ProjectNameContainer>
        <span>{projectName}</span>
      </ProjectNameContainer>
    </Block>
  );
};
